# 02 - Create App

# 02.01 - Create a new App
1. Create a new App "Home" in your Django application.
2. Create a simple HTTPResponse in your views.py file.
3. Include the new view at your default empty url path
4. register you App in your settings.py file
5. Run your Server and test if everything works.

**Your Response should be just "Hello World"**
