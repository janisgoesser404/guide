# 09 - DRF

# 08.04 Authentication & User Registration
1. Use the Django URL Authentication for Userlogin. Test it with your Postman Program.
2. Create an API where a user gets created. Test your API with your Postman Program. There are many Tutorials out there. Please use them if you have problems on that task. **Please Note** For that task you may will need to overwrite your UserSerializer. Most of the popular Tutorials on YouTube are working with the serializer for that task. And it's important to get the Concepts behind that.
