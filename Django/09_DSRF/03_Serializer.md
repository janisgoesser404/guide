# 09 - DRF

# 08.03 Serializers Part 1
Create a new file serializers.py in your Application ToDo. Just copy the following lines into the file:
```
from rest_framework import serializers
from .models import Task

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'
```

Import that serializer in your views.py file and work in your views.py again. Copy following lines to your code:
```
from .serializers import TaskSerializer

@api_view(['GET',])
def get_all_tasks(request):
    tasks = Task.objects.all()
    serializer = TaskSerializer(tasks, many=True)
    return Response(serializer.data)
```

And register that view in your urls.py file.

Open this API now in
1. your Browser
2. Postman

**And now your turn:**
1. change the serializer - display just the fields id, title, body, creation date, user who is responsible for that task
2. Look at your Response by using Postman again.
3. Create a serializer which is called `UserSerializer` it should serialize the User Model and contains the following informations: username, email, first_name, last_name
4. Use the new `UserSerializer` for the user serialization at your `TaskSerializer` - this is called a *nested serializer* (if you have problems on this task, just google for it)
