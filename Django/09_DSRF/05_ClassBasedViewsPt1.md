# 09 - DRF

# 08.05 Class Based Views (CBS) Part 1
**Please Note** The CBS are one of the biggest pros of the DRF. You will save a lot of work if you spend time on understanding the Concepts behind Class based views.

**Please test every step in your Postman Software and verify that your steps are finished.**

1. Generate a simple ListView which shows maximum 10 Tasks (The user don't have to be Authenticated)
2. Change your simple ListView which shows maximum 10 Tasks just to an authenticated User)
3. Change your simple ListView which shows maximum 10 Tasks for a User who is required for that task. Therefor you may will need to overwrite the `get_queryset` method
4. Allow to Create New Task in your API.
5. The Created Task should automatically be created by the user who is logged in and is calling this API. Therefor you may will ned to overwrite the `perform_create` Method
6. Allow to mark a task to "Done" by put request. But: Only the person who is responsible for that task should be able to mark it to done.
