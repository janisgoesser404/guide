# 09 - DRF

# 08.02 Creating Function Based Views (FBV)
First a little warmup. In Task 06_02 you should have created a JSONResponse. I want you to change this function to a Function based get Response by the Django Rest Framework (DRF). Please use the following imports:
```
from rest_framework.decorators import api_view
from rest_framework.response import Response
```

and use the `api_view(['GET',])` decorator for your function. Change the `JSONResponse` to the new response you have imported. We don't need the JSON Response anymore.

Test this API by two ways:
1. open it in your Browser
2. open it in Postman
