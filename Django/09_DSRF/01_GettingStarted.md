# 09 - DRF
**Please Note** Here is where functions become very interesting and important for our project! In the Frontend a JavaScript Framework will be in used. And we will develop our application completely separated from the Front-end application. You will really need to understand how an API works and you will need to know the differences between Function Based and Class based views. You will need to Work with the Generics and Mixins of DRF. Please take a special focus to this section!

# 08.01 Getting Started
**Please Note** You do not need to install new modules by pip. If you have installed the requirements sucessfully all packages which are needed are already installed.
- Register your Django Rest Framework in your Settings file
- Allow a Token Authentication - therefor you will need to register something in your settings file as well and you will need to make a new migration
- You will have some Problems with corsheades. For that example project it's completely okay to allow all corsheaders. If you have any permission problems, just remember this point and google for it.

**Tipp** The following link is the best link for learning DRF I have ever seen (even better than the official Documentation) -- **you will learn to love it as soon as you are working with Class Based Views:**

http://www.cdrf.co