# 09 - DRF

# 08.06 Class Based Views (CBV) Part 1
**Please Note** The CBV are one of the biggest pros of the DRF. You will save a lot of work if you spend time on understanding the Concepts behind Class based views.

**Please test every step in your Postman Software and verify that your steps are finished.**

1. Create a new Admin API for your Tasks. The it should be only possible for an admin to access this API.
2. Create a user filter - the Api should returns the newest 10 tasks for a user who is searched
3. Allow to delete a task by given id
