# 04 - Admin

# 04.01 - Working with the Admin File
1. Register your model Task in your admin.py file
2. Get Access to your admin panel and create your first task manually
3. Create a additional TaskAdmin Class at your admin.py and register it
4. edit the columns which are displayed in the admin overview of your tasks. Show:
	- `__str__`
	- done information
	- creation date
	- done date
	- user who created that task
5. Make the done information in your overview editable
6. create Search fields for the username who is responsible for that task, the username who created this task and a title
7. create a filter where you can Filter for tasks which are done and for your categories.
8. display the not editable fields in your detail view
