# 04 - Admin

# 04.02 - Signals
**Please note**: This section Should be in the Category Models. But for the Workflow it's better to integrate it in that step. That task is not needed for the future functionality of the application! But it gives you useful and interesting skills.

The Goal should be, that you'll send automatically an email to a person who is responsible for a task if he is not the creator of this task.

1. Make an Account at Sendgrid.com - you will get ask for a Company name. Just use imagine names. And please use the free version! You will be able to send 100 Mails per day. Sendgrid Guides you through the portal. You will need to verify your email address and give them your real contact information for using the service. If you have done everything use their API Guide for Python. Put the generated API Key in your .env file!
2. Create a post save signal. If a task was created proof if the creator is equal to the responsible person for that task. If they are not equal, send an email with body "You have a new task. Look at your profile at our portal!". You need to create a new user at your platform for finishing that task.
