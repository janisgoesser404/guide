# 05 - Shell

# 05.01 - Shell
**Please Note** This Section should gives you an idea of how you can work with Django and test your application in a pretty low level way. It's not relevant for the functionality for this project.

It might be a challenge to use the line by line tool from Python. You can use a simple Texteditor and copy your scripts into your shell as well.

1. Print out all Usernames of the registered users
2. Create a new user at your platform
3. Change your password by using the shell. Try to log in at the admin panel with your new password
4. Create a new Task
5. Filter for all Tasks which are not done yet
6. Filter for all tasks which are not older than 1 hour.
