# 01 - Getting Started
**Please Note** This Django Guide is one Guide to create a whole app. Each task depends on a Task before. At the End you should have created. We are focused on the Backend Development. At the end you will have created a To do Ticket System where you can assign Tasks to different users. The final Result will be a full Rest Framework without a Userfrontend.

# 01.01 - Getting Started
1. Create a virtual environment and install the given requirements.txt file (it's separate from this file)
2. Start the virtualenv
3. Create a new Django Project "Ticket System"
4. Create a new Postgres Database `ticketdb`
5. Create a .env File which contains the database Credentials (simply take a look at the .env.example File)
6. Include the Databasecredentials in your setting.py file
7. run `python manage.py migrate` and `python manage.py createsuperuser` and of course `python manage.py runserver`
8. Get Access to http://127.0.0.1:8000/admin/

Everything should have worked until here.
