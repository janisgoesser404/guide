# 08 - Postman

# 08.01 Postman
**Please Note** This is not a Skill specially for your Django Software. But you will need Postman to test your API in a very simple and quick way. It's a great tool where you can also share your endpoints to every person of your project. Great for working together - great for everything!
1. Download the Postman Tool
2. Create a get Request to one of your urls
3. Create a new Collection - call it "To Do"
