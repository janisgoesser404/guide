# 03 - Models

# 03.01 - Working with Models
1. Create a new App "To Do" - don't forget to register it to your settings file!
2. create a Model "Task". It should have
- Foreign Key to the User who should do this task
- Charfield for a title
- Choicefield named category with possible values "programming", "bugfixing", "something"
- textfield for a description which is not required
- boolean field with an information if the task is done (default Value: False)
- a datetimefield when the task was created (it should be automatically filled with now, and it should not be editable)
- a datetimefield when the task was marked as done (it can be empty and should not be editable)
- a Foreign key to the user who created this task
3. Create a `__str__` method. It should returns the title of the task, the username who is responsible for that task and if the task is done or not
4. Migrate your database
