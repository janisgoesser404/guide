# 06 - Views

# 06.01 - Templates
1. Create a tasks.html file in a template directory of your App To Do. Just give it a header `<h1>To Do</h1>` and create a view function which returns the template in the response. You'll need to register your view in your urls.py file as well.
2. Filter for all tasks of your database and put it to the context of your created view function. List every title of the to dos in the body of your created template as a list like:
```
<ul>
  <li>To Do Title 1</li>
  <li>To Do Title 2</li>
  ....
  <li>To Do Title n</li>
</ul>
```
3. filter for all tasks which are assigned to the user who is authenticated at the moment. If the user is not authenticated, he should se no tasks. Display them in the body of your template.
