# 06 - Views

# 06.02 JSON Response
**Please note** This task is not really necessary for the functionality of the final result. You will be able to finish that program without doing that task. But it shows important principles of Webdevolpment in general and is important to understand.

Create a new view function which returns a dictionary like the following:
```
tasks = {
	1:{'title':'To Do 1', 'done':False, 'responsible_username':'janis', 'created_by_username':'Ulrich'},
	2:{'title':'To Do 2', 'done':False, 'responsible_username':'janis', 'created_by_username':'Ulrich'},
	3:{'title':'To Do 3', 'done':False, 'responsible_username':'Eric', 'created_by_username':'Janis'},
	4:{'title':'To Do 4', 'done':True, 'responsible_username':'Eric', 'created_by_username':'Ulrich'}
}
```

The function should creates a JSON Response with that dictionary.
Register it in your urls.py file and open the url in your browser.
