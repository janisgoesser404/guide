# 07 - Urls

# 07.01 Urls
**Please note** This task is not really necessary for the functionality of the final result. You will be able to finish that program without doing that task. But it shows important principles of Django and is important to understand.

If you have just one urls.py file at all, please create in your apps a new urls.py file. This files should only refers to the view methods which are in the app where they are created for.

Remove your old references of your views now from the existing urls.py file in your settings directory and include the additional urls.py files instead.

If you have no experience for that, watch this video:
https://www.youtube.com/watch?v=AOcWELTI56Q
