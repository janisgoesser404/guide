# 10 - Testing

# 10.01 Testing
**Please Note** This is not necessary for the functionality of your program. But tests are important skills and sometimes a dealbreaker to get on new projects. If you have the Time to, please just try to create some simple unit and api tests.

**TaskModelTest**
- Assert an error by a missing required field
- test if the creation date is always filled automatically
- test if the donetime field get filled automatically by marking the to do as done. *You will need to fix your model in this case. Because the time for done is always empty at this moment ;)*

It should be okay if you go until here. If you have the time: Please add some Unit tests to your model. You could also test your API. This will be a bit more complicated. But it's also important.
