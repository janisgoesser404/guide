# 08 Modules

# 08.01 - Own Modules
Write a file with simple geometric classes:
1. Rectangle - Properties: x,y,height, width, methods: area, perimeter, test if point is in rect
2. Circle - Properties: radius, central point x, central point y. methods: area, permiter, test if point is in circle

Write a new file. Import your rectangle and circle class into your file. Create a Rectangle and Circle with the parameters you want and test if the point [3,4] is in your circle / rectangle.
