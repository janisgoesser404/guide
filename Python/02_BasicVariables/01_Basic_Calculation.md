# 02 Basic Variables

## 02.01 - Basic Calculation
Given are the variables ```a=4``` and ```b=2```.
1. Calculate the following results:

 - r_1 = a+b
 - r_2 = a-b
 - r_3 = a*b
 - r_4 = a/b
 - r_5 = a%b
 - r_6 = a^b
 - r_7 = a^(1/b) (Note, this is the sqrt of the number)

2. Create for each r a print which gives the result and the type of the result.
3. r_4 should be a float even if 4 / 2 = 2. You have 2 possibilities to generate an integer of that result. Please use one of that possibilities.
