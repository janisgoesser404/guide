# 02 Basic Variables

## 02.02 - Working with prints and Strings
Given are the follow variables:
- `name = Tim`
- `age = 23`
- `post_code = 01998`
- `work_is_done = 35.5`

Please generate The following output:

> The employee Tim with age 23 and living place at post code 01998 has 35.5 percent of his work done now.

Do it in 4 ways:
- using the C/C++ Style by using % Operator in String
- using a comma separated string
- using the old Python Style with the format method
- using the new Python Style with the f-String Method (Note this is running just for Python > 3.6)
