# 03 Lists

## 03.01 - Basic List work with Strings
1. Generate a list within the following Names:

> Max, Christian, Josh, Lea, Dolores, Mira

2. Write a script which gives you the information how many names are in your list.
3. Write a print which gives you the Name ```Christian```
4. Create a simple print which gives you the last element of the list (don't use a static number for that!)
5. Create a simple print which gives you the list between position 1 and 4
6. Create a simple print which gives you all the List Members until position 4
7. Create a simple print which gives you all the List Members higher than position 1
8. Write a Script which tests if a special name is in the list.
9. Add the name ```Florian``` to the list.
