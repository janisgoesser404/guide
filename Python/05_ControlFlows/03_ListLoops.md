# 05 ControlFlows

## 05.03 - List Loops
The following List is given:
`l_1 = ['Max', 'Christian', 'Josh', 'Lea', 'Dolores', 'Mira']`
1. Write a loop which prints every name of that list
2. Write a loop which prints every name and the index where it stands

Another list is given:
`l_2 = [1,2,3,2,2,5,7,8,4,3,9,9,11,5,6,1,1,1,1]`
3. The prime number from 1 to 20 are: 1,2,3,5,7,11,13,17,19. Test if each number of the List `l_2` is a prime.
4. Create a new List `l_3`. Fill that with the values of `l_2` - but make every value unique in that list.
