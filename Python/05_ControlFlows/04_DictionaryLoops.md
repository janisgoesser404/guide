# 05 ControlFlows

## 05.04 - Dictionary Loops
The following dictionary is given:
```
d = {
	'Max':{'age':23, 'city':'Berlin'},
	'Lea':{'age':30, 'city':'London'},
	'Ben':{'age':35, 'city':'Madrid'}
}
```

Iterate through each element in that dictionary. Generate for every row an output like the following:

> Max is 23 years old and lives in Berlin
