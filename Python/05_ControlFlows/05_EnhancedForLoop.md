# 05 ControlFlows

## 05.05 - Enhanced For Loops
Please use for the following tasks the enhanced for loop in Python.

The following List is given:
`l_1 = [1,2,3,2,2,5,7,8,4,3,9,9,11,5,6,1,1,1,1]`

1. create a new list `l_2` which contains all numbers of list `l_1` if the number is grater than 5.

The following dictionary is given:
```
d = {
	'Max':{'age':23, 'city':'Berlin'},
	'Lea':{'age':30, 'city':'London'},
	'Ben':{'age':35, 'city':'Madrid'},
	'James':{'age':24, 'city':'Barcelona'},
	'Lisa':{'age':21, 'city':'Sydney'},
	'Laura':{'age':41, 'city':'New York'},
}
```
2. create a list `l_3` which contains all names of the dictionary if the person is older or equal than 30 years.
