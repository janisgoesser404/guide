# 05 ControlFlows

## 05.01 - Basic For Loop
1. Create a for loop from 0 to 100 with step width = 1. Print for every number which is divisible by the number 3: ```divisible by number 3``` if the number is not divisible by number 3 print ```not divisible by number 3```
2. Create a for loop which counts from 0 to 100 with step width 3
3. Create a for loop which counts from 100 to 1 back with step width 1
4. Write a basic prime number test. The variable ```x=?``` is given. Write a for loop from 2 to x-1. If there is a number which divides x , x is no prime number -> you can break the for loop. If there is no number which divides x, x is a prime number. Print the result if x is a prime number or not.
