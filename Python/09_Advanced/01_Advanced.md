# 09 Advanced

# 09.01 - Advanced
1. Given is a list `l = [1,2,3,2,2,5,7,8,4,3,9,9,11,5,6,1,1,1,1]` - create a list which contains just unique Elements by using the map method.
2. Given is a dictionary:
```
d = {
	'Max':{'age':23, 'city':'Berlin'},
	'Lea':{'age':30, 'city':'London'},
	'Ben':{'age':35, 'city':'Madrid'},
	'James':{'age':24, 'city':'Barcelona'},
	'Lisa':{'age':21, 'city':'Sydney'},
	'Laura':{'age':41, 'city':'New York'},
}
```
create a list `l_2` which contains all names of the dictionary if the person is older or equal than 30 years. Use again the map method.
