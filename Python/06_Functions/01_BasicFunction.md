# 06 Functions

## 06.01 - Basic Function
Create a function `basic` it should get the following:
- a number
- addition value (default: 5)
- maximum value (default: 100)

The Function should do the following:
As long as the number is smaller or equal the maximum value, add the addition value to the number.
The function should return the number of iteration which where needed for the process.

Please make sure: the addition value needs to be greater than 0.
The maximum Value needs tu be grater than 0.
If these conditions are false by input, raise a ValueError
