# 01 Getting Started

## 01.01 - Setup

Check your Python Version on your local system by typing

Mac / Ubuntu
```
python3 --version
```

Windows
```
python --version
```

If you not get a valid response, you'll need to download Python3
