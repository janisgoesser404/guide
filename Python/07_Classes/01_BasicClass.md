# 07 Classes

# 07.01 - Basic Class Example
1. Write a Python class named `Circle` constructed by a radius and two methods which will compute the area and the perimeter of a circle.

2. This task deals with the binary tree. Such tasks are popular examples in interviews for hiring new employees. The best way to work on this task is to have a look at this [graphic](https://www.geeksforgeeks.org/binary-tree-data-structure/). The graphic is a good way to explain the binary tree. Each number n has two children. For the child on the left 'n*2' applies, for the child on the right 'n*2 + 1'. There are other methods to explain the binary tree (e.g. using even and odd values). For this example, however, this should be sufficient.

A class that basically describes this binary tree could be represented as follows:
 ```
class BinaryTreeElement:
    def __init__(self, value):
        self.value = value

    @property    
    def left_value(self):
        return BinaryTreeElement(self.value * 2)

    @property
    def right_value(self):
        return BinaryTreeElement(self.value * 2 + 1)

    def __repr__(self):
        return repr(self.value)

b = BinaryTreeElement(3)
print(b)
print(b.left_value)
print(b.right_value)

```

Add a method to the class that specifies each parent element of any starting point.
For example: The end value 10 has the parents 5,2,1
To complete this task you may adapt the given class.

If you are not familiar with the notation, you can also choose the following class as a starting point:

```
class BinaryTreeElement:
    def __init__(self, value):
        self.value = value

    def left_value(self):
        return BinaryTreeElement(self.value * 2)

    def right_value(self):
        return BinaryTreeElement(self.value * 2 + 1)

b = BinaryTreeElement(3)

print(b.value)
print(b.left_value().value)
print(b.right_value().value)
```

3. This task makes less strict specifications. The content is a bit simpler than the binary tree.

The goal is to create the categorization of a shop system. For this purpose a class 'Category' is to be created. Just go to any major online shops - e.g. About You, Zalando and Co. and have products from any category displayed. You should notice relatively quickly a nesting of categories. e.g. Men -> shoes -> sneakers -> ...
The goal is to start from any category and then to close all parent elements.

This task is very similar to the binary tree. However, no class is given here as an aid.
